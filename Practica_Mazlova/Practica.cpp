#include <Windows.h>
#include <iostream>

HINSTANCE l_hInstance;
int l_nCmdShow;
HWND l_mainWnd;
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
BOOL InitAppClass();
BOOL InitWindow();
WPARAM StartMessageLoop();

int start_row = -1;
int start_column = -1;
int width = -1;
int height = -1;
COLORREF bg_color = -1;
HBITMAP hBitmap = NULL;

bool loadImage()
{
	hBitmap = (HBITMAP)LoadImage(NULL, L".\\LR1.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE | LR_CREATEDIBSECTION);

	if (!hBitmap)
		return false;

	BITMAP bm;
	GetObject(hBitmap, sizeof(BITMAP), &bm);

	uint8_t* data = (uint8_t*)bm.bmBits;

	int fill_color = -1;

	int min_figure_row = -1;
	int max_figure_row = -1;
	int min_figure_col = -1;
	int max_figure_col = -1;

	for (int row = 0; row < bm.bmHeight; ++row)
	{
		int col_min = -1;
		int col_max = -1;

		for (int col = 0; col < bm.bmWidth; ++col)
		{
			uint8_t& r = data[row * bm.bmWidth * 3 + col * 3];
			uint8_t& g = data[row * bm.bmWidth * 3 + col * 3 + 1];
			uint8_t& b = data[row * bm.bmWidth * 3 + col * 3 + 2];

			int rgb = ((int)b << 16) | ((int)g << 8) | (int)r;

			if (fill_color == -1)
				fill_color = rgb;

			const bool is_fill = rgb == fill_color;

			if (!is_fill)
			{
				if (col_min < 0)
					col_min = col;
			}
			else
			{
				if (col_min > 0 && col_max < 0)
					col_max = col - 1;

				r = bg_color & 0xFF;
				g = (bg_color & 0xFF00) >> 8;
				b = (bg_color & 0xFF0000) >> 16;
			}
		}

		if (col_min > 0)
		{
			if (min_figure_row < 0)
				min_figure_row = row;

			const int col_d = col_max - col_min;
			if (col_d > max_figure_col - min_figure_col)
			{
				min_figure_col = col_min;
				max_figure_col = col_max;
			}
		}
		else
		{
			if (min_figure_row > 0 && max_figure_row < 0)
				max_figure_row = row - 1;
		}
	}

	width = max_figure_col - min_figure_col;
	height = max_figure_row - min_figure_row;
	start_row = min_figure_row;
	start_column = min_figure_col;

	return true;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	l_hInstance = hInstance;
	l_nCmdShow = nCmdShow;

	if (!InitAppClass())
		return 0;
	if (!InitWindow())
		return 0;

	return StartMessageLoop();
}

BOOL InitAppClass()
{
	ATOM class_id;
	WNDCLASS wc;
	memset(&wc, 0, sizeof(wc));
	wc.lpszMenuName = NULL;
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = (WNDPROC)WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = l_hInstance;
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wc.lpszClassName = L"Practika_Mazlova";
	class_id = RegisterClass(&wc);
	if (class_id != 0)
		return TRUE;
	return FALSE;

}

BOOL InitWindow()
{

	l_mainWnd = CreateWindow(L"Practika_Mazlova", L"Mazlova Yelena", WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT,
		600, 600, 0, 0, l_hInstance, 0);

	if (!l_mainWnd)
		return FALSE;

	ShowWindow(l_mainWnd, l_nCmdShow);
	UpdateWindow(l_mainWnd);

	return TRUE;
}

WPARAM StartMessageLoop()
{
	MSG msg;
	while (1)
	{
		if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				break;
			DispatchMessage(&msg);
		}
	}
	return msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
	case WM_DESTROY:
	{
		PostQuitMessage(0);
		DeleteObject(hBitmap);
		return 0;
	}
	case WM_PAINT:
	{
		PAINTSTRUCT     ps;
		HDC             hdc;
		BITMAP          bitmap;
		HDC             hdcMem;
		HGDIOBJ         oldBitmap;
		RECT r;

		GetClientRect(hWnd, &r);

		hdc = BeginPaint(hWnd, &ps);

		if (bg_color == -1)
		{
			bg_color = GetPixel(hdc, 0, 0);

			if (!loadImage())
				return 0;
		}

		int width_to_out = width;
		int height_to_out = height;

		if (!(r.right > width && r.bottom > height))
		{
			double ratio = (double)width_to_out / (double)height_to_out;

			if ((double)r.right / width > (double)r.bottom / height)
			{
				height_to_out = r.bottom;
				width_to_out = ratio * height_to_out;
			}
			else
			{
				width_to_out = r.right;
				height_to_out = width_to_out / ratio;
			}
		}

		int topleft_x = 0;
		int topleft_y = r.bottom - height_to_out;

		hdcMem = CreateCompatibleDC(hdc);
		oldBitmap = SelectObject(hdcMem, hBitmap);

		GetObject(hBitmap, sizeof(bitmap), &bitmap);

		StretchBlt(hdc, topleft_x, topleft_y, width_to_out, height_to_out, hdcMem, start_column, bitmap.bmHeight - (start_row + height), width, height, SRCCOPY);

		SelectObject(hdcMem, oldBitmap);
		DeleteDC(hdcMem);

		EndPaint(hWnd, &ps);
		break;
	}
	default:
		break;
	}
	return DefWindowProc(hWnd, msg, wParam, lParam);
}
